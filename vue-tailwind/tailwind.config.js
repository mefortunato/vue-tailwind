module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      'display': ['Kreon', 'ui-open-sans']
    },
    backgroundPosition: {
      'bottom-neg-5': 'center bottom -5rem',
    },
    extend: {
      backgroundImage: () => ({
        'landing': "url('https://images.unsplash.com/photo-1524985069026-dd778a71c7b4?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=3902&q=80')"
      })
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
