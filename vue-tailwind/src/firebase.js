import firebase from "firebase/app";
import 'firebase/auth'
import 'firebase/firestore'

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDcMeNuXSCzPFlHzuiqS_dXmjX9VnY34OE",
  authDomain: "mytrackertv.firebaseapp.com",
  projectId: "mytrackertv",
  storageBucket: "mytrackertv.appspot.com",
  messagingSenderId: "1076980532304",
  appId: "1:1076980532304:web:9707ade210872541963d55",
  measurementId: "G-NC44KV9JH8"
};
firebase.initializeApp(firebaseConfig)

const db = firebase.firestore()
const auth = firebase.auth()

// collection references
const usersCollection = db.collection('users')

// export utils/refs
export {
  db,
  auth,
  usersCollection
}
