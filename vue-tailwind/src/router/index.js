import { createWebHistory, createRouter } from 'vue-router'
import { auth } from '../firebase'
import About from "@/views/About.vue"
import Counter from "@/views/Counter.vue"
import Home from "@/views/Home.vue"
import Login from "@/views/Login.vue"
import PageNotFound from "@/views/PageNotFound.vue"
import Profile from "@/views/Profile.vue"

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/counter",
    name: "Counter",
    component: Counter,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/:catchAll(.*)",
    name: "Page not found",
    component: PageNotFound
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(x => x.meta.requiresAuth)

  if (requiresAuth && !auth.currentUser) {
    next('/login')
  } else {
    next()
  }
})

export default router;