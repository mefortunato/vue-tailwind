import { ref } from 'vue'

const counter = ref(0)

export default function useCounter() {

  const increaseCounter = () => {
    counter.value++
  }

  const decreaseCounter = () => {
    counter.value = Math.max(0, counter.value - 1)
  }

  return {
    counter,
    increaseCounter,
    decreaseCounter
  }
}
